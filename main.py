from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains
import pandas as pd
import re
import time

PATH="D:\chromedriver.exe"
driver=webdriver.Chrome(PATH)
driver.get("https://demoqa.com/books")
driver.find_element_by_id("login").click()

driver.find_element_by_id("userName").send_keys("isratJerin")
driver.find_element_by_id("password").send_keys("Israt12%")
driver.find_element_by_id("login").click()
driver.maximize_window()

#WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//*[@id='see-book-Git Pocket Guide']/a"))).click()
#driver.refresh()
#driver.execute_script("scroll(0,250)")
#time.sleep(5)
#driver.execute_script("Document.getElementsByClassName('text-right')[0].click()")
#new_record=WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "text-right']")))
#driver.execute_script("arguments[0].click();", new_record)
#print(len(new_record))
table=[my_elem.text for my_elem in WebDriverWait(driver, 20).
      until(EC.visibility_of_all_elements_located((By.CSS_SELECTOR, "div.rt-tr-group div.rt-td")))]
title_author=[]
for td in table:
      if table.index(td)%4==1 or table.index(td)%4==2:
            title_author.append(td)

print(title_author)
df=pd.DataFrame(title_author)
df.to_csv('table.csv', index=False)

def write_description(book,fname):
      WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, book))).click()
      preface=WebDriverWait(driver, 20).until(EC.element_to_be_clickable(
            (By.XPATH,"/html/body/div/div/div/div[2]/div[2]/div[1]/div[2]/div[7]/div[2]/label"))).text
      f=open(fname,'w')
      f.write(preface)
      driver.get("https://demoqa.com/books")


write_description("//*[@id='see-book-Git Pocket Guide']/a",'book1.txt')
write_description("//*[@id='see-book-Learning JavaScript Design Patterns']/a",'book2.txt')

#for matching operations

matched=[]
def check(string):
    regex = re.compile("a\w*")
    match_object = regex.findall(string)
    if len(match_object) != 0:
        for word in match_object:
            c = 0
            matched.append(word)
            matched.append(word[::-1])
            for w1,w2 in zip(word, word[::-1]):
                if w1==w2:
                   c+=1
            percentage=(c/len(word))*100
            per_string=format(percentage,".2f")
            matched.append(str(per_string)+"%")

    else:
        print("No match")

with open('book1.txt', 'r') as file:
    book1 = file.read()
with open('book2.txt', 'r') as file:
    book2 = file.read()

check(book1)
check(book2)
print(matched)
df=pd.DataFrame(matched)
df.to_csv('letters_with_a.csv', index=False)

driver.close()






